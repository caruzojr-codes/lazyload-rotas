# Lazy Load e Rotas
Como realizar carregamento de módulos e componentes por etapas, conforme as rotas acessadas.


## Run Project
Clone este repositório para a máquina local
```
git clone https://caruzojr@bitbucket.org/caruzojr/lazyload-rotas.git
```
Dentro da pasta do projeto, execute o comando abaixo para baixar todas as dependencias necessárias
```
npm install
```
Rode o seguinte comando para executar o projeto
```
ng s --open
```
