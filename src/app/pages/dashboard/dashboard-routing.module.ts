import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RelatoriosComponent } from './relatorios/relatorios.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard/relatorios', pathMatch: 'full' },
  { path: 'relatorios', component: RelatoriosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
