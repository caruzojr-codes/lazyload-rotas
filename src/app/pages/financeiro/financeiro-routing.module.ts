import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExtratoComponent } from './extrato/extrato.component';

const routes: Routes = [
  { path: '', redirectTo: '/financeiro/extrato', pathMatch: 'full' },
  { path: 'extrato', component: ExtratoComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceiroRoutingModule { }
