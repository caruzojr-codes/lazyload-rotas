import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceiroRoutingModule } from './financeiro-routing.module';
import { ExtratoComponent } from './extrato/extrato.component';


@NgModule({
  declarations: [ExtratoComponent],
  imports: [
    CommonModule,
    FinanceiroRoutingModule
  ]
})
export class FinanceiroModule { }
